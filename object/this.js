var a = 10;
console.log(this);
// this mengembalikan isi (object) window



// literal
var mhs = {a: 10, b: 20};
mhs.halo = function() {
    console.log(this);
    console.log("Hai!");
};
mhs.halo();
// this mengembalikan object bersangkutan


// function declaration
function halo() {
    console.log(this);
    console.log("Halo!");
};
this.halo();
halo();
// this mengembalikan object global


// constructor
function Hai() {
    console.log(this);
    console.log("Haii!");
};
new Hai();

var obj1 = new Hai();
var obj2 = new Hai();
// this mengembalikan object yang baru dibuat
