// object literal
var mhs = {
    nama: "Mahardika",
    nim: 120863,
    umur: 17,
    alamat : {
        desa: "Belalang",
        kecamatan: "Kediri",
        provinsi: "Bali"
    }
};

// menggunakan function declaration
function buatObjectMahasiswa(nama, nim, jurusan) {
    var mhs = {};
    mhs.nama = nama;
    mhs.jurusan = jurusan;
    mhs.nim = nim;
    return mhs;
}
var mhs2 = buatObjectMahasiswa("Anto", 9876341, "Teknik Pertukangan");
var mhs3 = buatObjectMahasiswa("Andi", 8766321, "Teknik Informatika");

// Constructor
function Mahasiswa(nama, nim, jurusan) {
    this.nama = nama;
    this.nim = nim;
    this.jurusan = jurusan;
};
var mhs4 = new Mahasiswa("Mahardika", 902843924, "Teknik ajalah");