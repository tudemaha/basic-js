var nyawa = 3;
var random = Math.floor(Math.random() * 10 + 1);
var input;
var gagal = true;

alert("Tebak angka 1-10\nKamu punya " + nyawa + " kali kesempatan!");

while(nyawa > 0) {
    input = parseInt(prompt("Masukkan angka tebakan:"));

    nyawa--;
    if(input < random) alert("Tebakan terlalu RENDAH\nAda " + nyawa + " kali kesempatan lagi!");
    else if(input > random) alert("Tebakan terlalu TINGGI\nAda " + nyawa + " kali kesempatan lagi!");
    else {
        alert("Tebakan BENAR!");
        gagal = false;
        break;
    }
}

if(nyawa === 0 && gagal) alert("Kesempatan habis, angka yang ditebak adalah " + random + ".");
alert("Terima kasih telah memainkan game ini.");