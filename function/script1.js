function jumlahVolumeDuaKubus(sisi1, sisi2) {
    return (sisi1 * sisi1 * sisi1) + (sisi2 * sisi2 * sisi2);
    // ini sudah menggunakan refactoring, hanya mengalikan tanpa membuat variabel baru
}

sisi1 = 10;
sisi2 = 15;

alert(jumlahVolumeDuaKubus(sisi1, sisi2));