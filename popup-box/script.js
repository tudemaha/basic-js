/* 
Popup Box di JS:
alert
prompt
confirm
*/

// // alert => hanya pemberitahuan
// alert("halo");
// alert("nama");
// alert("saya");
// alert("Mahardika Adi Putra");

// // prompt => bisa meminta input
// var nama = prompt("Masukkan nama:");
// alert(nama);

// confirm => melakukan konfirmasi (mengembalikan boolean)
var setuju = confirm("Apakah anda yakin?");
if(setuju) {
    alert("User menekan OK");
} else {
    alert("User menekan Cancel");
}