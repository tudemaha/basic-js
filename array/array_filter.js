var angka = [8, 1, 3, 9, 10, 3, 5];

// filer (dapat mengembalikan lebih dari satu nilai)
var angka2 = angka.filter(function(e) {
    return e > 5;
});
console.log(angka2);

// find (hanya mengembalikan satu nilai)
var angka3 = angka.find(function(f) {
    return f > 5;
});
console.log(angka3);