var angka = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
var nama = ["Anto", "Budi", "Yanto"];

// // for loop
// for(var i = 0; i < angka.length; i++) {
//     console.log(angka[i]);
// }

// foreach
angka.forEach(function(e) {
    console.log(e);
});

nama.forEach(function(e, i) {
    console.log("Mahasiswa ke-" + (i + 1) + " adalah " + e);
});

// map (mengambalikan aray)
nama.map(function(e) {
    console.log(e);
});

// map dapat digunakan seperti ini untuk memanipulasi setiap elemen array
var angka2 = angka.map(function(e) {
    return e * 2;
});
console.log(angka2.join(", "));