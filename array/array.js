// menambah isi array
var arr1 = ["a", 1, true];
// console.log(arr1[1]);

var arr2 = []
arr2[0] = "Mahardika";
arr2[1] = "Adi";
arr2[3] = "Putra";

// menghapus isi array
var arr3 = ["Sandhika", "Galih"];
// arr3[0] = undefined;
// console.log(arr3)

// menampilkan isi array
for(var i = 0; i < arr3.length; i++) {
    console.log(arr3[i]);
}