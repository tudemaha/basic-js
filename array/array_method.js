// length (mengetahui jumlah elemen pada array)

// join (menggabungkan semua elemen array dan dijadikan array)
var arr = ["Mahardika", "Adi", "Putra", "Andi"];
console.log(arr.join(', '))

// push (menambah elemen di akhir array) => bisa menambahkan beberapa elemen
arr.push("Bagus")
console.log(arr.join(', '))

// pop (menghapus elemen terakhir di array)
arr.pop();
console.log(arr.join(', '))

// unshift (menambah elemen baru di awal array)
arr.unshift("Anjay");
console.log(arr.join(', '))

// shift (menghapus elemen pertama di array)
arr.shift();
console.log(arr.join(', '))

// splice (menambahkan elemen di tengah)
// splice(indexAwal, berapaDihapus, elemen1, elemen2,...)
arr.splice(3, 0, "Andre", "Budi");
console.log(arr.join(', '))

// slice (mengambil beberapa bagian pada array)
// slice(awal, akhir - 1)
console.log(arr.slice(1, 3));

// sort (sort array)
var angka = [3, 2, 6, 9, 10, 22, 12];
console.log(angka.join(", "));

angka.sort()    // akan mengurutkan dari karakter pertama terlebih dahulu
console.log(angka.join(", "));

angka.sort(function(a, b) { // mengurutkan sesuai dengan angka matematika
    return a - b;
});
console.log(angka.join(", "));